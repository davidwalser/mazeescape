# MazeEscape
The goal of this game is to reach the final destination (marked as green) as soon as possible.

## Controls
- W / Arrow Up ... move forward
- A / Arrow Left ... move left
- S / Arrow Down ... move backward
- D / Arrow Right ... move right

## Gameplay Features
- A timer measures the "score"

## Technical Features (Unity and other)
- Reading files with UnityStandaloneFileBrowser
- dynamically instantiating the maze
- You can create your own maze in a .txt file and import that into the game


## Time Spent report
Overall I think I achieved what I wanted to achieve, allthought there are still some key features missing. For example you cannot exit the game through buttons.
| Feature/Work | Planned Time / h | Actual Time / h |
|--------------|------------------|-----------------|
| Read File | 1 | 1 |
| Main Menu | 2 | 2 |
| Player | 2.5 | 3 |
| Instantiate Maze | 4 | 4 |
| Creating Maze Files | 0.5 | 0.5 |
| Documentation | 1 | 1 |
| Game logic | 1 | 0.5 |

## Problems and Challenges
I wasn't able to start the particle system, triggerd by entering the finnish. Also I had no time left creating a "exit to main menu" button / pause menu, so you are kind of trapped in the game. Even if you find the finish, only the score stops. The game is also only playable in 1920x1080.

## Resources, Sources, References and Links
https://github.com/gkngkc/UnityStandaloneFileBrowser

## Self Assessment
 - working game build, overall impression, is it a game?: 2
 - project structure: 3
 - documentation: 4
 - features:
    - Materials: 3
    - Light: 2
    - Particles: 5 (see Problems)
    - Audio: 1
    - Input: 3