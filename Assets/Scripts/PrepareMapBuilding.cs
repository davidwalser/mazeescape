﻿using SFB;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PrepareMapBuilding : MonoBehaviour {

    public Text NameOfFile;
    private bool isValidMaze = false;

    // Use this for initialization
    void Start () {
    }

    // Update is called once per frame
    void Update () {
	}

    public void LoadFile()
    {
        var pathOfMazeFile = StandaloneFileBrowser.OpenFilePanel("Open File", "", "", false);
        string [] mazeText = System.IO.File.ReadAllLines(pathOfMazeFile[0]);
        isValidMaze = MazeTextIsValid(mazeText);
        if (isValidMaze)
        {
            NameOfFile.text = pathOfMazeFile[0];
            StaticClass.CrossSceneInformation = mazeText;
        }
        else
        {
            NameOfFile.text = "Error";
        }
    }

    private bool MazeTextIsValid(string [] mazeText)
    {
        if (mazeText == null) return false;
        bool containsStart = false, containsFinnish = false;
        foreach (var line in mazeText)
        {
            foreach (var character in line.ToLower().ToCharArray())
            {
                if (character == 's') containsStart = true;
                if (character == 'f') containsFinnish = true;
            }
        }
        return containsStart && containsFinnish;
    }

    public void LoadNextLevel()
    {
        if (isValidMaze)
        {
            SceneManager.LoadScene("Game");
        }
        else
        {

        }

    }
}

public static class StaticClass
{
    public static string [] CrossSceneInformation { get; set; }
}
