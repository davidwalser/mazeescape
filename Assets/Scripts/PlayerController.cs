﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

    [Range(1, 20)]
    public float MovementSpeed = 1;
    public float JumpForce = 2;
    public float Gravity = 2;

    private float fallSpeed = 4;


    // Use this for initialization
    void Start()
    {
        

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey("up") || Input.GetKey("w"))
        {
            transform.position += new Vector3(0, 0, 0.1f * MovementSpeed);
        }
        if (Input.GetKey("left") || Input.GetKey("a"))
        {
            transform.position += new Vector3(-0.1f * MovementSpeed, 0, 0);
        }
        if (Input.GetKey("down") || Input.GetKey("s"))
        {
            transform.position += new Vector3(0, 0, -0.1f * MovementSpeed);
        }
        if (Input.GetKey("right") || Input.GetKey("d"))
        {
            transform.position += new Vector3(0.1f * MovementSpeed, 0, 0);
        }
        if (Input.GetKey("space"))
        {
            transform.position += new Vector3(0, 0.2f * 2, 0f);
        }
        fallSpeed += Gravity * Time.deltaTime;
        transform.position += new Vector3(0, -fallSpeed, 0) * Time.deltaTime;

        //Debug.Log(fallSpeed);
        //Debug.DrawRay(transform.position, transform.up * -1 * 10, Color.green);
        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.up * -1, out hit, maxDistance: 10))
        {
            var distance = Vector3.Distance(transform.position, hit.point);
            if (distance > 0)
            {
                transform.position = hit.point + new Vector3(0, 1, 0);
                fallSpeed = 0;
            }
        }
        //left
        if (Physics.Raycast(transform.position, transform.right * -1, out hit, maxDistance: 10))
        {
            var distance = Vector3.Distance(transform.position, hit.point);
            if (distance < 0.5f)
            {
                transform.position = hit.point + new Vector3(0.5f, 0, 0);
            }
        }
        //right
        if (Physics.Raycast(transform.position, transform.right, out hit, maxDistance: 10))
        {
            var distance = Vector3.Distance(transform.position, hit.point);
            if (distance < 0.5f)
            {
                transform.position = hit.point - new Vector3(0.5f, 0, 0);
            }
        }
        //forward
        if (Physics.Raycast(transform.position, transform.forward, out hit, maxDistance: 10))
        {
            var distance = Vector3.Distance(transform.position, hit.point);
            if (distance < 0.5f)
            {
                transform.position = hit.point - new Vector3(0, 0, 0.5f);
            }
        }
        //backward
        if (Physics.Raycast(transform.position, transform.forward * -1, out hit, maxDistance: 10))
        {
            var distance = Vector3.Distance(transform.position, hit.point);
            if (distance < 0.5f)
            {
                transform.position = hit.point - new Vector3(0, 0, -0.5f);
            }
        }
    }
}
