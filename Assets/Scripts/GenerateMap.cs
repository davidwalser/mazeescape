﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.UI;
using Debug = UnityEngine.Debug;

public class GenerateMap : MonoBehaviour {

    public GameObject GroundCube;
    public GameObject WallCube;
    public GameObject StartCube;
    public GameObject FinishCube;
    public GameObject PlayerPrefab;
    private GameObject Player;
    private Vector3 StartPoint;
    private Vector3 EndPoint;
    private string[] mazeText;
    public float distanceToFinnish = 0.5f;
    private TextMesh timeText;
    private Stopwatch stopwatch;
    // Use this for initialization
    void Start () {
        mazeText = StaticClass.CrossSceneInformation;
        CreateMaze();
        Player = GameObject.Instantiate(PlayerPrefab, StartPoint, Quaternion.identity);
        stopwatch = new Stopwatch();
        stopwatch.Start();
        timeText = Player.GetComponentInChildren<TextMesh>();
        Debug.Log(timeText.name);
    }
	
	// Update is called once per frame
	void Update () {
        if (DetectIfWon())
        {
            stopwatch.Stop();
			ParticleSystem particleSystem = FinishCube.GetComponentInChildren<ParticleSystem> ();
			Debug.Log (particleSystem.name);
			//todo fix particle system and make exit to main menu button
			particleSystem.Play ();
			Debug.Log (particleSystem.isPlaying);
        }
        //Debug.Log(stopwatch.ElapsedMilliseconds);
        timeText.text = "Score: " + stopwatch.ElapsedMilliseconds.ToString();
	}

    private void CreateMaze()
    {
        for (int z = 0; z < mazeText.Length; z++)
        {
            char[] symbols = mazeText[z].ToLower().ToCharArray();
            for (int x = 0; x < symbols.Length; x++)
            {
                InstantiateFromSymbol(symbols[x], x, z);
            }
        }
    }

    private bool DetectIfWon()
    {
        if (Vector3.Distance(Player.transform.position,EndPoint) <= distanceToFinnish) 
        {
            return true;
        }
        return false;
    }


    public void InstantiateFromSymbol(char symbol, float x, float z)
    {
        switch (symbol)
        {
            case '#': GameObject.Instantiate(WallCube, new Vector3(x,1,z),Quaternion.identity); break;
            case ' ': GameObject.Instantiate(GroundCube, new Vector3(x,0,z),Quaternion.identity); break;
            case 's': GameObject.Instantiate(StartCube, new Vector3(x,0,z),Quaternion.identity); StartPoint = new Vector3(x, 1, z); break;
            case 'f': GameObject.Instantiate(FinishCube, new Vector3(x,0,z),Quaternion.identity); EndPoint = new Vector3(x, 1, z); break;
            default: break;
        }
    }
}
